<?php
	define('IN_PHPBB', true);
	$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : '../';
	$phpEx = substr(strrchr(__FILE__, '.'), 1);
	include($phpbb_root_path . 'common.' . $phpEx);
	include_once("../includes/db/mysqli.php");
	include_once("../includes/acp/acp_forums.php");
	
	function output($data, $level = 0)
	{
		for ($i = 0; $i < $level; $i++)
		{
			echo "&nbsp;&nbsp;";
		}
		echo $data . "<hr />";
	}
	
	$forumControl = new acp_forums();
	$db = new dbal_mysqli();
	
	$states = array();
	$counter = 0;
	$states_fhandle = fopen("states.lst", 'r');
	$stateSets = array();
	while ($state = fgets($states_fhandle, 4096))
	{
		$states[$counter] = $state;
		$stateSets[$state] = array();
		$counter++;
	}
	fclose($states_fhandle);
	
	foreach ($states as $state)
	{
		$counties_fhandle = fopen($state . ".lst", 'r');
		$counter = 0;
		while ($county = fgets($counties_fhandle))
		{
			$stateSets[$state][$counter] = $county;
			$counter++;
		}
		fclose($counties_fhandle);
	}
	
	$id = $db->sql_connect("localhost", "sambb", "clanexo", "sambb");
	
	//echo("<h1>" . $id . "</h1>");
	
	foreach ($states as $state)
	{
		output("Processing " . $state . "...");
		$fname = utf8_normalize_nfc($state . " Discussion");
		
		$forum_data = array(
			'parent_id'				=> 6,
			'forum_type'			=> FORUM_POST,
			'forum_status'			=> ITEM_UNLOCKED,
			'forum_parents'			=> '',
			'forum_name'			=> $fname,
			'forum_link'			=> '',
			'forum_link_track'		=> false,
			'forum_desc'			=> '',
			'forum_rules'			=> '',
			'forum_rules_link'		=> '',
			'forum_image'			=> '',
			'forum_style'			=> 0,
			'display_subforum_list'	=> true,
			'display_on_index'		=> false,
			'forum_topics_per_page'	=> 0,
			'enable_indexing'		=> true,
			'enable_icons'			=> false,
			'enable_prune'			=> false,
			'prune_days'			=> 7,
			'prune_viewed'			=> 7,
			'prune_freq'			=> 1,
			'forum_flags'			=> FORUM_FLAG_POST_REVIEW + FORUM_FLAG_ACTIVE_TOPICS,
			'forum_options'			=> 0,
			'forum_password'		=> '',
			'forum_password_confirm'=> '',
		);
		
		create_forum($forum_data);
		$fid = $db->sql_nextid();
		
		if ($fid != null && $fid > 0)
		{
			output("Forum " . $fid . " created", 1);
		}
		
		$count = 0;
		foreach ($stateSets[$state] as $county)
		{
			output("Processing " . $county . "...", 1);
			if ($count == 0)
			{			
				$fname = utf8_normalize_nfc($county . " Discussion");
				
				$forum_data["parent_id"] = $fid;
				$forum_data["forum_name"] = $fname;
				
				create_forum($forum_data);
			}
			$count++;
		}
	}
	
	function create_forum($forum_data)
	{
		global $db, $phpbb_root_path;
		
		$forum_data['forum_flags'] = 0;
		$forum_data['forum_flags'] += ($forum_data['forum_link_track']) ? FORUM_FLAG_LINK_TRACK : 0;
		$forum_data['forum_flags'] += ($forum_data['prune_old_polls']) ? FORUM_FLAG_PRUNE_POLL : 0;
		$forum_data['forum_flags'] += ($forum_data['prune_announce']) ? FORUM_FLAG_PRUNE_ANNOUNCE : 0;
		$forum_data['forum_flags'] += ($forum_data['prune_sticky']) ? FORUM_FLAG_PRUNE_STICKY : 0;
		$forum_data['forum_flags'] += ($forum_data['show_active']) ? FORUM_FLAG_ACTIVE_TOPICS : 0;
		$forum_data['forum_flags'] += ($forum_data['enable_post_review']) ? FORUM_FLAG_POST_REVIEW : 0;
		$forum_data['forum_flags'] += ($forum_data['enable_quick_reply']) ? FORUM_FLAG_QUICK_REPLY : 0;

		// Unset data that are not database fields
		$forum_data_sql = $forum_data;

		unset($forum_data_sql['forum_link_track']);
		unset($forum_data_sql['prune_old_polls']);
		unset($forum_data_sql['prune_announce']);
		unset($forum_data_sql['prune_sticky']);
		unset($forum_data_sql['show_active']);
		unset($forum_data_sql['enable_post_review']);
		unset($forum_data_sql['enable_quick_reply']);
		unset($forum_data_sql['forum_password_confirm']);
		
		
		// Find correct ID
		if (true)
		{
			$sql = 'SELECT left_id, right_id, forum_type
				FROM ' . FORUMS_TABLE . '
				WHERE forum_id = ' . $forum_data_sql['parent_id'];
			$result = $db->sql_query($sql);
			$row = $db->sql_fetchrow($result);
			$db->sql_freeresult($result);

			if ($row['forum_type'] == FORUM_LINK)
			{
				$errors[] = $user->lang['PARENT_IS_LINK_FORUM'];
				return $errors;
			}

			$sql = 'UPDATE ' . FORUMS_TABLE . '
				SET left_id = left_id + 2, right_id = right_id + 2
				WHERE left_id > ' . $row['right_id'];
			$db->sql_query($sql);

			$sql = 'UPDATE ' . FORUMS_TABLE . '
				SET right_id = right_id + 2
				WHERE ' . $row['left_id'] . ' BETWEEN left_id AND right_id';
			$db->sql_query($sql);

			$forum_data_sql['left_id'] = $row['right_id'];
			$forum_data_sql['right_id'] = $row['right_id'] + 1;
		}
		
		$sql = 'INSERT INTO ' . FORUMS_TABLE . ' ' . $db->sql_build_array('INSERT', $forum_data_sql);
		$db->sql_query($sql);

		$forum_data['forum_id'] = $db->sql_nextid();
		
		return $errors;
	}
?>